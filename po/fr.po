# French translations for hibernate@dafne.rocks package.
# Copyright (C) 2021 THE hibernate@dafne.rocks'S COPYRIGHT HOLDER
# This file is distributed under the same license as the hibernate@dafne.rocks package.
# Nicolás Hermosilla <nhermosilla14@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: hibernate@dafne.rocks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-16 00:00+0800\n"
"PO-Revision-Date: 2021-03-29 02:02-0300\n"
"Last-Translator: Nicols Hermosilla <nhermosilla14@gmail.com>\n"
"Language-Team: French <traduc@traduc.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ASCII\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Create hibernate action item
#: src/extension.js:60
msgid "Hibernate"
msgstr "Mettre en hibernation"
